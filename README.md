# World Time

**Version 1.0.0**

**Flutter 1.20**

This project was created in Flutter using Dart and represent a list of countries which you can select to see the time for each of them. I developed this app to improve my skills on Flutter framework and Dart language and to play around with some widgets and API's.

##                                                            VIEW DEMO !

![Demo](demo/demo.gif)

## Contributors
@ Florentin Lupascu <lupascu_florentin@yahoo.com>

## License & Copyright
© Florentin Lupascu, WorldTime

Licensed under the [MIT License](LICENSE).
